package model;

import javafx.concurrent.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class EncryptFilesTask extends Task<List<String>> {

    private List<File> fileList;
    private List<String> errorList;
    private String masterPW;
    private boolean deleteFile;
    private EnDecryptor encryptorDecryptor;
    private boolean enCryptionToggle;
    private boolean bigFile;
    private boolean checkListBeforeEncrypt;

    public EncryptFilesTask(List<File> list, String masterPW, EnDecryptor encryptorDecryptor, boolean encryptionToggle, boolean deleteFile, boolean bigFile, boolean checkListBeforeEncrypt) {
        Thread.currentThread().setName("EncryptorTool - helpertask (EncryptFiles)");
        this.fileList = list;
        this.deleteFile = deleteFile;
        this.masterPW = masterPW;
        this.encryptorDecryptor = encryptorDecryptor;
        this.enCryptionToggle = encryptionToggle;
        this.bigFile = bigFile;
        this.checkListBeforeEncrypt = checkListBeforeEncrypt;

        errorList = new ArrayList<>();
    }

    @Override
    protected List<String> call() {
        this.updateProgress(-1f, -1f);
        this.updateMessage("Setting up ...\n");

        if (checkListBeforeEncrypt) {
            this.updateMessage("Filtering files that don't need to be ");
            if (enCryptionToggle) {
                this.updateMessage("encrypted\n");
            } else {
                this.updateMessage("decrypted\n");
            }

            for (int i = 0; i < fileList.size(); i++) {
                File file = fileList.get(i);
                String filename = file.getName();
                if (enCryptionToggle) {
                    if (filename.endsWith(".encrypted")) {
                        fileList.remove(file);
                        this.updateMessage(filename + " was omitted\n");
                    }
                } else {
                    if (!filename.endsWith(".encrypted")) {
                        fileList.remove(file);
                        this.updateMessage(filename + " was omitted\n");
                    }
                }
            }
        }

        fileList.sort((o1, o2) -> (int) (o2.length() - o1.length()));

        long count = (long) fileList.size();
        long workDone = 0;


        int errorCounter = 0;

        if (count > 20) {
            this.updateMessage("More than 20 files detected, moving to multithreading.\n");
            if (enCryptionToggle) {
                encryptorDecryptor.multiThreadedEncryptFile(fileList, masterPW, deleteFile, bigFile);
            } else {
                encryptorDecryptor.multiThreadedDecryptFile(fileList, masterPW, deleteFile);
            }
        } else {
            this.updateMessage("Starting\n");
            updateProgress(0, count);

            for (File file : this.fileList) {
                try {
                    workDone++;
                    if (enCryptionToggle) {
                        this.updateMessage("\nEncrypting " + file.getName() + " ...");
                        encryptorDecryptor.encryptFile(file, masterPW, deleteFile, bigFile);
                        this.updateMessage(" Successfully encrypted!");
                    } else {
                        this.updateMessage("\nDecrypting " + file.getName() + " ...");
                        encryptorDecryptor.decryptFile(file, masterPW, deleteFile);
                        this.updateMessage("  Successfully decrypted!");
                    }
                } catch (Exception e) {
                    this.updateMessage(" An error occurred: " + e.getMessage());
                    errorCounter++;
                    errorList.add(String.format("An error occured while encrypting %s: %s", file.getName(), e.getMessage()));
                } finally {
                    updateProgress(workDone, count);
                }
            }
        }

        this.masterPW = "";

        if (enCryptionToggle) {
            this.updateMessage("\n\nEncrypting files completed with " + errorCounter + " error(s)");
        } else {
            this.updateMessage("\n\nDecrypting files completed with " + errorCounter + " error(s)");
        }

        return errorList;
    }
}


