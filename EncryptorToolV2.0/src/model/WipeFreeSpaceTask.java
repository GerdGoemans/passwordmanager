package model;

import javafx.concurrent.Task;

import java.io.*;
import java.util.List;

public class WipeFreeSpaceTask extends Task<List<String>> {

    private File drive;
    private boolean run;

    public WipeFreeSpaceTask(File drive) {
        Thread.currentThread().setName("EncryptorTool - helpertask (WipeFreeSpace)");
        this.drive = drive;
        run = true;
    }

    @Override
    protected List<String> call() throws IOException {
        updateProgress(-1f, -1f);
        updateMessage("Starting");

        /*Check if it's a drive and not a folder*/
        boolean driveCheck = false;
        for (File file : File.listRoots()) {
            if (file.equals(drive)) driveCheck = true;
        }
        if (!driveCheck) {
            throw new IllegalArgumentException(drive.getName() + "is not a root drive.");
        }

        /*Space checks and some defines*/
        double originalFreeSpace = drive.getFreeSpace();
        double currentFreeSpace = originalFreeSpace;
        double arraySize = originalFreeSpace / 1000;
        byte[] tempArray = new byte[(int) arraySize];

        updateMessage("Wiping free space ...");
        updateProgress(originalFreeSpace - currentFreeSpace, originalFreeSpace);

        File tempFile = new File(drive.getAbsolutePath() + "0.tmp");
        try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tempFile)))) {

            /*Write the majority of the disk*/
            while (currentFreeSpace > arraySize * 4 && run) {
                for (int i = 0; i < 4; i++) {
                    dos.write(tempArray);
                    dos.flush();
                    currentFreeSpace = drive.getFreeSpace();
                    updateProgress(originalFreeSpace - currentFreeSpace, originalFreeSpace);
                }
            }
            /*write the remainder of the disk*/
            if (run) {
                currentFreeSpace = drive.getFreeSpace();
                dos.write(new byte[(int) (currentFreeSpace - (currentFreeSpace / 100))]);
                dos.flush();
            }
            updateMessage("Finishing ...");
            updateProgress(-1.0, -1.0);

            dos.close();
            boolean fileDeleted = tempFile.delete();
            if (!fileDeleted) {
                throw new IOException("File failed to delete");
            }

        } catch (Exception e) {
            boolean fileDeleted = tempFile.delete();
            if (!fileDeleted) {
                throw new IOException("File failed to delete");
            }
            throw e;
        }

        return null;
    }

    public void haltTask() {
        run = false;
    }
}
