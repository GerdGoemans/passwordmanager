package model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static model.FileFunctions.getPathSeperator;
import static model.FileFunctions.getSaveLocation;

public class MasterPasswordManager {

    public static void saveMasterPassword(byte[] masterPassword, byte[] salt) throws IOException {
        Path saveFile = Paths.get(String.format("%s%s%s.bin", getSaveLocation().toString(), getPathSeperator(), "HashedAndSaltedMasterPW"));
        Files.deleteIfExists(saveFile);
        Files.createFile(saveFile);
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(saveFile.toFile())));

        dos.writeInt(masterPassword.length);
        dos.write(masterPassword);
        dos.flush();
        dos.writeInt(salt.length);
        dos.write(salt);
        dos.flush();
        dos.close();
    }

    public static byte[][] loadMasterPassword() throws IOException {
        Path saveFile = Paths.get(String.format("%s%s%s.bin", getSaveLocation().toString(), getPathSeperator(), "HashedAndSaltedMasterPW"));
        DataInputStream dos = new DataInputStream(new BufferedInputStream(new FileInputStream(saveFile.toFile())));
        int pwLength = dos.readInt();
        byte[] password = new byte[pwLength];
        dos.readFully(password);

        int saltLength = dos.readInt();
        byte[] salt = new byte[saltLength];
        dos.readFully(salt);
        dos.close();

        return new byte[][]{password, salt};
    }

    public static boolean masterPasswordExists() throws IOException {
        Path saveFile = Paths.get(String.format("%s%s%s.bin", getSaveLocation().toString(), getPathSeperator(), "HashedAndSaltedMasterPW"));
        return Files.exists(saveFile);
    }

}
