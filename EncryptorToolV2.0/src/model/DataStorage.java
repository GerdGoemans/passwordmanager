package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DataStorage {

    private String name;
    private String content;
    private String unEncryptedContent;

    private byte[] iv;
    private byte[] salt;

    @Expose(serialize = false)
    private static List<DataStorage> dataStorageList = new ArrayList<>();
    @Expose(serialize = false)
    private static String saveLocation = null;

    public DataStorage() throws IllegalArgumentException, UnsupportedOperationException, NullPointerException, ClassCastException {
        if (dataStorageList.contains(this)) {
            throw new IllegalArgumentException("This element already exists");
        } else {
            dataStorageList.add(this);
        }
    }

    public DataStorage(String name, String content, boolean isEncrypted) throws IllegalArgumentException, UnsupportedOperationException, NullPointerException, ClassCastException{
        this.name = name;
        if (isEncrypted) {
            this.content = content;
        } else {
            this.unEncryptedContent = content;
        }

        if (dataStorageList.contains(this)) {
            throw new IllegalArgumentException("This element already exists");
        } else {
            dataStorageList.add(this);
        }
    }

    /*Get & set*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (dataStorageList.contains(this)) throw new IllegalArgumentException("Given list already exists");
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.unEncryptedContent = null;
        this.content = content;
    }

    byte[] getIv() {
        return iv;
    }

    void setIv(byte[] iv) {
        this.iv = iv;
    }

    byte[] getSalt() {
        return salt;
    }

    void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public static List<DataStorage> getDataStorageList() {
        return dataStorageList;
    }

    private static void setDataStorageList(List<DataStorage> dataStorageList) {
        DataStorage.dataStorageList = dataStorageList;
    }

    public String getUnEncryptedContent() {
        return unEncryptedContent;
    }

    public void setUnEncryptedContent(String unEncryptedContent) {
        this.content = null;
        this.unEncryptedContent = unEncryptedContent;
    }

    /*Other methods*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataStorage that = (DataStorage) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public String toString() {
        return "DataStorage{" +
                "name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", unEncryptedContent='" + unEncryptedContent + '\'' +
                ", iv=" + Arrays.toString(iv) +
                ", salt=" + Arrays.toString(salt) +
                '}';
    }

    public static void removeDataStorage(DataStorage dataStorage) {
        dataStorageList.remove(dataStorage);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void saveDataStorages() throws IOException {
        dataStorageList.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String jsonString = gson.toJson(dataStorageList);

        File file = new File(getSaveLocation());
        if (file.exists()) file.delete();
        file.createNewFile();

        try (FileWriter jsonWriter = new FileWriter(file)) {
            jsonWriter.write(jsonString);
        }
    }

    public static void loadDataStorages() throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        File file = new File(getSaveLocation());
        if (!file.exists()) {
            setDataStorageList(new ArrayList<>());
            return;
        }

        try (BufferedReader data = new BufferedReader(new FileReader(file))) {
            DataStorage[] dataStorages = gson.fromJson(data, DataStorage[].class);
            List<DataStorage> arrayList = new ArrayList<>(Arrays.asList(dataStorages));
            setDataStorageList(arrayList);
        }
    }

    private static String getSaveLocation() throws IOException {
        if (saveLocation == null) {
            saveLocation = FileFunctions.getSaveLocation().toString() + FileFunctions.getPathSeperator() + "DataStorage.json";
        }
        return saveLocation;
    }
}
