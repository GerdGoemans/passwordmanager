package model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileFunctions {

    static String getPathSeperator() {
        return System.getProperty("file.separator");
    }

    public static Path getSaveLocation() throws IOException {
        Path savePath = Paths.get(System.getProperty("user.home") + getPathSeperator() + "AppData" + getPathSeperator() + "Local" + getPathSeperator() + "EncryporToolData");
        if (Files.notExists(savePath)) Files.createDirectories(savePath);
        return savePath;
    }

}
