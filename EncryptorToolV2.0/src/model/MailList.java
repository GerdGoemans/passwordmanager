package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class MailList {

    private String mail;

    @Expose(serialize = false)
    private static List<MailList> mailList;
    @Expose(serialize = false)
    private static String saveLocation = null;

    public MailList(String email) {
        this.mail = email;
        if (getMailList().size() == 0) setMailList(new ArrayList<>());
        getMailList().add(this);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public static List<MailList> getMailList() {
        return mailList;
    }

    public static void setMailList(List<MailList> mailList) {
        MailList.mailList = mailList;
    }

    @Override
    public String toString() {
        return getMail();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailList mailList = (MailList) o;
        return Objects.equals(mail, mailList.mail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mail);
    }

    public static void loadMailList() throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        File file = new File(getSaveLocation());
        if (!file.exists()) {
            setMailList(new ArrayList<>());
            return;
        }

        try (BufferedReader data = new BufferedReader(new FileReader(file))) {
            MailList[] mails = gson.fromJson(data, MailList[].class);
            List<MailList> tempMails = new ArrayList<>(Arrays.asList(mails));
            setMailList(tempMails);
        } catch (IOException e) {
            throw e;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void saveMailList() throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String jsonString = gson.toJson(getMailList());

        File file = new File(getSaveLocation());
        if (file.exists()) file.delete();
        file.createNewFile();

        try (FileWriter jsonWriter = new FileWriter(file)) {
            jsonWriter.write(jsonString);
        } catch (IOException e) {
            throw e;
        }
    }

    private static String getSaveLocation() throws IOException {
        if (saveLocation == null) {
            saveLocation = FileFunctions.getSaveLocation().toString() + FileFunctions.getPathSeperator() + "MailList.json";
        }
        return saveLocation;
    }
}
