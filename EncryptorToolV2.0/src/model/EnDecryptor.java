package model;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class EnDecryptor {

    /*File methods*/
    void multiThreadedEncryptFile(List<File> fileList, String password, boolean deleteFile, boolean bigFile) {
        fileList.parallelStream().forEach(file -> {
            try {
                encryptFile(file, password, deleteFile, bigFile);
            } catch (Exception e) {/*Ignore*/}
        });
    }

    void multiThreadedDecryptFile(List<File> fileList, String password, boolean deleteFile) {
        fileList.parallelStream().forEach(file -> {
            try {
                decryptFile(file, password, deleteFile);
            } catch (Exception e) {/*Ignore*/}
        });
    }

    void decryptFile(File file, String password, boolean deleteFile) throws Exception {
        if (file == null || !file.exists()) throw new IllegalArgumentException("Given file does not exist");

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));

        int length = dis.readInt();
        byte[] data = new byte[length];
        dis.readFully(data);
        String type = new String(data, UTF_8);

        switch (type) {
            case "bigFile":
                decryptBigFile(dis, file, password, deleteFile);
                break;
            case "normal":
                decryptNormalFile(dis, file, password, deleteFile);
                break;
            default:
                throw new IllegalArgumentException("File " + file.getName() + " could not be decrypted because its type was not recognized.");
        }
    }

    private void decryptBigFile(DataInputStream dis, File file, String password, boolean deleteFile) throws Exception {

        String path = file.getAbsolutePath();
        path = path.substring(0, path.lastIndexOf(".encrypted"));

        File newFile = new File(path);
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(newFile)));

        int saltLength = dis.readInt();
        byte[] salt = new byte[saltLength];
        dis.readFully(salt);

        int ivLength = dis.readInt();
        byte[] iv = new byte[ivLength];
        dis.readFully(iv);

        SecretKey key = generateKey(password, salt);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));

        double numOfEncryptRnds = dis.readDouble();

        for (double i = 0; i < numOfEncryptRnds; i++) {

            int length = dis.readInt();

            byte[] toDecrypt = new byte[length];
            //noinspection ResultOfMethodCallIgnored
            dis.read(toDecrypt);

            byte[] decryptedBytes = cipher.doFinal(toDecrypt);
            dos.write(decryptedBytes);
            dos.flush();
        }

        dis.close();
        dos.close();

        if (deleteFile) {
            boolean returnDelete = file.delete();
            if (!returnDelete) throw new Exception("File: " + file.getName() + " failed to delete");
        }
    }

    private void decryptNormalFile(DataInputStream dis, File file, String password, boolean deleteFile) throws Exception {
        String path = file.getAbsolutePath();
        path = path.substring(0, path.lastIndexOf(".encrypted"));

        int saltLength = dis.readInt();
        byte[] salt = new byte[saltLength];
        dis.readFully(salt);

        int ivLength = dis.readInt();
        byte[] iv = new byte[ivLength];
        dis.readFully(iv);

        int contentLength = dis.readInt();
        byte[] inputBytes = new byte[contentLength];
        dis.readFully(inputBytes);
        dis.close();

        SecretKey key = generateKey(password, salt);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] outputBytes = cipher.doFinal(inputBytes);

        if (deleteFile) {
            boolean returnDelete = file.delete();
            if (!returnDelete) throw new Exception("File: " + file.getName() + " failed to delete");
        }
        file = new File(path);

        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        dos.write(outputBytes);
        dos.close();
    }

    void encryptFile(File file, String password, boolean deleteFile, boolean bigFile) throws Exception {

        if (file == null) throw new IllegalArgumentException("File was null");

        if (bigFile) {
            encryptBigFile(file, password, deleteFile);
        } else {
            encryptNormalFile(file, password, deleteFile);
        }

    }

    private void encryptBigFile(File file, String password, boolean deleteFile) throws Exception {
        String newFilePath = file.getAbsolutePath();
        newFilePath = newFilePath + ".encrypted";

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));

        long fileLength = file.length();

        byte[] salt = generateSalt();
        SecretKey key = generateKey(password, salt);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();

        File newFile = new File(newFilePath);
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(newFile)));

        String str = "bigFile";
        byte[] data = str.getBytes(UTF_8);
        dos.writeInt(data.length);
        dos.write(data);

        dos.writeInt(salt.length);
        dos.write(salt);
        dos.flush();

        dos.writeInt(iv.length);
        dos.write(iv);
        dos.flush();

        double numOfEncryptRnds = Math.ceil(fileLength / 5000000.0);
        dos.writeDouble(numOfEncryptRnds);
        dos.flush();

        for (double i = 0; i < numOfEncryptRnds; i++) {
            byte[] toEncrypt = new byte[5000000];

            int result = dis.read(toEncrypt);
            if (result == -1)
                throw new IllegalArgumentException("Something went wrong while encrypting file: " + file.getName());
            byte[] outputBytes = cipher.doFinal(toEncrypt);

            dos.writeInt(outputBytes.length);
            dos.write(outputBytes);
            dos.flush();
        }
        dos.close();
        dis.close();

        if (deleteFile) {
            boolean returnDelete = file.delete();
            if (!returnDelete) throw new Exception("File: " + file.getName() + " failed to delete");
        }
    }

    private void encryptNormalFile(File file, String password, boolean deleteFile) throws Exception {

        String newFilePath = file.getAbsolutePath();
        newFilePath = newFilePath + ".encrypted";

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));

        byte[] inputBytes = new byte[(int) file.length()];
        dis.readFully(inputBytes);
        dis.close();

        byte[] salt = generateSalt();
        SecretKey key = generateKey(password, salt);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();

        byte[] outputBytes = cipher.doFinal(inputBytes);

        if (deleteFile) {
            boolean returnDelete = file.delete();
            if (!returnDelete) throw new Exception("File: " + file.getName() + " failed to delete");
        }
        File newFile = new File(newFilePath);

        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(newFile)));

        String str = "normal";
        byte[] data = str.getBytes(UTF_8);
        dos.writeInt(data.length);
        dos.write(data);

        dos.writeInt(salt.length);
        dos.write(salt);
        dos.flush();

        dos.writeInt(iv.length);
        dos.write(iv);
        dos.flush();

        dos.writeInt(outputBytes.length);
        dos.write(outputBytes);
        dos.flush();
        dos.close();
    }

    /*Datastorage methods*/
    public void encryptDataStorage(String password, DataStorage dataStorage) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidParameterSpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        if (dataStorage.getUnEncryptedContent() == null)
            throw new IllegalArgumentException("Cannot encrypt - no uncencrypted data found");
        if (dataStorage.getSalt() == null) {
            dataStorage.setSalt(generateSalt());
        }

        SecretKey key = generateKey(password, dataStorage.getSalt());
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        dataStorage.setIv(iv);

        byte[] toEncrypt = dataStorage.getUnEncryptedContent().getBytes(UTF_8);
        byte[] ciphertext = cipher.doFinal(toEncrypt);

        dataStorage.setContent(Base64.getEncoder().encodeToString(ciphertext));
    }

    public void decryptDataStorage(String password, DataStorage dataStorage) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException, InvalidAlgorithmParameterException {
        if (dataStorage.getIv() == null) {
            throw new IllegalArgumentException("IV is null");
        }
        if (dataStorage.getSalt() == null) {
            throw new IllegalArgumentException("Salt is null");
        }
        if (dataStorage.getContent() == null) {
            throw new IllegalArgumentException("No content found to decrypt");
        }

        SecretKey key = generateKey(password, dataStorage.getSalt());
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(dataStorage.getIv()));

        byte[] toDecryptBytes = Base64.getDecoder().decode(dataStorage.getContent());
        byte[] decryptedBytes = cipher.doFinal(toDecryptBytes);

        dataStorage.setUnEncryptedContent(new String(decryptedBytes, UTF_8));
    }

    /*misc methods*/
    private SecretKey generateKey(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    public static byte[] generateSalt() throws NoSuchAlgorithmException {
        byte[] salt = new byte[80];
        SecureRandom.getInstanceStrong().nextBytes(salt);
        return salt;
    }

    public byte[] hashMasterPassword(byte[] masterPassword, byte[] salt) throws NoSuchAlgorithmException {
        byte[] saltedPassword = new byte[masterPassword.length + salt.length];
        System.arraycopy(masterPassword, 0, saltedPassword, 0, masterPassword.length);
        System.arraycopy(salt, 0, saltedPassword, masterPassword.length, salt.length);

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(saltedPassword);
    }

    public String sha1Hash(String toHash) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] hash = digest.digest(toHash.getBytes(UTF_8));
        return byteArrayToHexString(hash);
    }

    private static String byteArrayToHexString(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

}
