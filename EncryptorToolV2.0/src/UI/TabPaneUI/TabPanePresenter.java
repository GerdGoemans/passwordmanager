package UI.TabPaneUI;

import UI.AboutUI.AboutPresenter;
import UI.AboutUI.AboutView;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.stage.Stage;
import model.DataStorage;
import model.FileFunctions;

import java.io.IOException;

import static UI.UIConstants.*;
import static model.DataStorage.getDataStorageList;

public class TabPanePresenter {

    private TabPaneView tabPaneView;
    private Stage stage;

    public TabPanePresenter(TabPaneView tabPaneView, Stage stage) {
        this.tabPaneView = tabPaneView;
        this.stage = stage;
        addEventHandlers();
    }

    private void addEventHandlers() {
        /*Menu item actions*/
        this.tabPaneView.getSaveMenuItem().setOnAction(event -> {
            try {
                setStatusBoxText("Saving ...");
                for (DataStorage dataStorage : getDataStorageList()) {
                    if (dataStorage.getUnEncryptedContent() != null) {
                        if (getMasterPassword().equals("")) {
                            if (!askUserForPassword()) return;
                        }

                        getEnDecryptor().encryptDataStorage(getMasterPassword(), dataStorage);
                    }
                }
                DataStorage.saveDataStorages();
                setStatusBoxText("Saving complete");
            } catch (Exception e) {
                Alert alert = alert(Alert.AlertType.ERROR, "Error", "An error occurred while saving:", e.getMessage());
                alert.show();
                setStatusBoxText("Saving failed");
            } finally {
                setMasterPassword("");
            }
        });
        this.tabPaneView.getAboutMenuItem().setOnAction(event -> {
            Stage secondaryStage = new Stage();
            AboutView aboutView = new AboutView();
            Scene aboutScene = new Scene(aboutView);
            new AboutPresenter(secondaryStage, aboutView, aboutScene);
            aboutScene.getStylesheets().add("CSS/General.css");
            secondaryStage.setScene(aboutScene);
            secondaryStage.setMaximized(false);
            secondaryStage.setTitle("About this program");
            secondaryStage.setWidth(500);
            secondaryStage.setHeight(400);
            secondaryStage.show();
        });
        this.tabPaneView.getSaveLocationItem().setOnAction(event -> {
            try {
                Alert alert = alert(Alert.AlertType.INFORMATION, "Save location", "Your files are saved in the following location", FileFunctions.getSaveLocation().toString());
                alert.getButtonTypes().clear();
                ButtonType copy = new ButtonType("Copy path");
                ButtonType close = new ButtonType("Close");
                alert.getButtonTypes().addAll(copy, close);

                alert.showAndWait();
                if (alert.getResult().equals(copy)) {
                    ClipboardContent clipboardContent = new ClipboardContent();
                    clipboardContent.putString(FileFunctions.getSaveLocation().toString());
                    Clipboard.getSystemClipboard().setContent(clipboardContent);
                }

            } catch (IOException e) {
                Alert alert1 = alert(Alert.AlertType.ERROR, "ERROR", "Something went wrong while putting the savepath in the clipboard", e.getMessage());
                alert1.show();
            }

        });
        this.tabPaneView.getExportMenuItem().setOnAction(event -> {
            Alert alert = alert(Alert.AlertType.INFORMATION, "Not implemented", "This function is not implemented", "");
            alert.show();
        });
        this.tabPaneView.getCloseMenuItem().setOnAction(event -> {
            Alert alert = alert(Alert.AlertType.CONFIRMATION, "Exit", "You're about to close the program", "Nothing will be saved, are you sure you want to close the program?");
            alert.getButtonTypes().clear();
            ButtonType exit = new ButtonType("Exit");
            ButtonType cancel = new ButtonType("Cancel");
            alert.getButtonTypes().addAll(cancel, exit);
            alert.showAndWait();

            if (alert.getResult().equals(exit)) {
                stage.close();
            } else {
                event.consume();
            }
        });
        //this.tabPaneView.getChangeMasterPWMenuItem().setOnAction(event -> {/*TODO*/});

    }

    private void setStatusBoxText(String text) {
        this.tabPaneView.getStatusLabel().setText(text);
    }
}
