package UI.TabPaneUI;

import UI.CleanDiskUI.CleanDiskPresenter;
import UI.CleanDiskUI.CleanDiskView;
import UI.FileEncryptUI.FileEncryptPresenter;
import UI.FileEncryptUI.FileEncryptView;
import UI.PasswordManagerUI.PasswordManagerPresenter;
import UI.PasswordManagerUI.PasswordManagerView;
import UI.RandomStringUI.RandomStringPresenter;
import UI.RandomStringUI.RandomStringView;
import UI.UIConstants;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

import static UI.UIConstants.getMainScene;

public class TabPaneView extends BorderPane {

    //general
    private Stage stage;
    private TabPane tabPane;

    //Menu bar
    private MenuBar menuBar;
    private MenuItem loadMenuItem;
    private MenuItem saveMenuItem;
    private MenuItem exportMenuItem;
    private MenuItem closeMenuItem;
    private MenuItem aboutMenuItem;
    private MenuItem saveLocationItem;
    private MenuItem changeMasterPWMenuItem;

    /*Bottom*/
    private GridPane bottomPane;
    private static Label statusLabel;

    public TabPaneView(Stage stage) throws IOException {
        this.stage = stage;
        UIConstants.setPrimaryStage(stage);
        UIConstants.setupStage(stage);
        initialiseNodes();
        layoutNodes();
        startAllTabs();
    }

    private void startAllTabs() throws IOException {
        PasswordManagerView passwordManagerView = new PasswordManagerView(stage);
        new PasswordManagerPresenter(passwordManagerView, stage);
        Tab mainTab = new Tab("Passwords");
        mainTab.setContent(passwordManagerView);

        Tab cleanTab = new Tab("Clean disk");
        Tab fileTab = new Tab("File encryption");
        Tab randomStringTab = new Tab("Random string generator");
        //Tab pwnedTab = new Tab("Have I been pwned?");
        //Tab mailTab = new Tab("Mail list");

        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == fileTab && fileTab.getContent() == null) {
                FileEncryptView fileEncryptView = new FileEncryptView();
                new FileEncryptPresenter(stage, fileEncryptView);
                fileTab.setContent(fileEncryptView);
            }
            if (newValue == cleanTab && cleanTab.getContent() == null) {
                CleanDiskView cleanDiskView = new CleanDiskView();
                new CleanDiskPresenter(stage, cleanDiskView, getMainScene());
                cleanTab.setContent(cleanDiskView);
            }
            if (newValue == randomStringTab && randomStringTab.getContent() == null) {
                RandomStringView randomStringView = new RandomStringView();
                new RandomStringPresenter(randomStringView);
                randomStringTab.setContent(randomStringView);
            }
            /*if (newValue == pwnedTab && pwnedTab.getContent() == null) {
                PwnedView pwnedView = new PwnedView();
                new PwnedPresenter(pwnedView);
                pwnedTab.setContent(pwnedView);
            }
            if (newValue == mailTab && mailTab.getContent() == null) {
                MailView mailView = new MailView();
                new MailPresenter(mailView);
                mailTab.setContent(mailView);
            }*/
        });

        tabPane.getTabs().addAll(mainTab, fileTab, randomStringTab, cleanTab/*, pwnedTab, mailTab*/);
    }

    private void initialiseNodes() {
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        /*Menu (top)*/
        loadMenuItem = new MenuItem("Load file");
        saveMenuItem = new MenuItem("Save");
        closeMenuItem = new MenuItem("Close application");
        exportMenuItem = new MenuItem("Export");
        changeMasterPWMenuItem = new MenuItem("Change master password");
        Menu fileMenu = new Menu("File", null,
                saveMenuItem, loadMenuItem, new SeparatorMenuItem(),
                exportMenuItem, new SeparatorMenuItem(),
                changeMasterPWMenuItem, new SeparatorMenuItem(),
                closeMenuItem);

        saveLocationItem = new MenuItem("Show save location");
        aboutMenuItem = new MenuItem("Open about menu");
        Menu aboutMenu = new Menu("About", null, saveLocationItem, aboutMenuItem);

        this.menuBar = new MenuBar(fileMenu, aboutMenu);

        /*bottom*/
        bottomPane = new GridPane();
        statusLabel = new Label("");
    }

    private void layoutNodes() {
        this.setMinHeight(200);
        double screenHeight = Screen.getPrimary().getBounds().getHeight();
        this.setMaxHeight(screenHeight - screenHeight / 4);
        this.setMinWidth(400);
        double screenWidth = Screen.getPrimary().getBounds().getWidth();
        this.setMaxWidth(screenWidth - screenWidth / 4);

        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(100);

        /*Top*/
        this.setTop(this.menuBar);

        /*Centre*/
        this.setCenter(tabPane);

        /*bottom*/
        bottomPane.add(statusLabel, 0, 0);
        bottomPane.setPadding(new Insets(4, 8, 4, 8));
        bottomPane.setHgap(2);
        bottomPane.setVgap(4);
        bottomPane.getColumnConstraints().add(col);

        this.setBottom(this.bottomPane);
    }

    MenuItem getSaveMenuItem() {
        return saveMenuItem;
    }

    MenuItem getExportMenuItem() {
        return exportMenuItem;
    }

    MenuItem getCloseMenuItem() {
        return closeMenuItem;
    }

    MenuItem getAboutMenuItem() {
        return aboutMenuItem;
    }

    public static Label getStatusLabel() {
        return statusLabel;
    }

    MenuItem getSaveLocationItem() {
        return saveLocationItem;
    }
}
