package UI.CleanDiskUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

public class CleanDiskView extends GridPane {

    private Label driveSelectLabel;
    private ChoiceBox<String> driveSelectChoiceBox;
    private Button cancelButton;
    private Button wipeButton;
    private ProgressIndicator progressIndicator;
    private Label statusLabel;

    public CleanDiskView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        driveSelectLabel = new Label("Select drive: ");
        driveSelectChoiceBox = new ChoiceBox<>();
        cancelButton = new Button("Cancel");
        wipeButton = new Button("Wipe free space");
        progressIndicator = new ProgressIndicator();
        statusLabel = new Label("");
    }

    private void layoutNodes() {
        ColumnConstraints col50 = new ColumnConstraints();
        col50.setPercentWidth(50);
        this.getColumnConstraints().addAll(col50, col50);

        this.setPadding(new Insets(4, 8, 4, 8));
        this.setHgap(4);
        this.setVgap(4);

        this.add(driveSelectLabel, 0, 0);
        this.add(driveSelectChoiceBox, 1, 0);

        this.add(wipeButton, 0, 2, 2, 1);

        GridPane.setHalignment(cancelButton, HPos.CENTER);
        GridPane.setHalignment(statusLabel, HPos.CENTER);
        GridPane.setHalignment(progressIndicator, HPos.CENTER);
        progressIndicator.setMinSize(50, 50);

        this.setMinWidth(200);
        this.setMinHeight(100);
    }

    void showProgressIndicator(boolean show) {
        if (show) {
            driveSelectChoiceBox.setDisable(true);

            this.add(progressIndicator, 0, 2, 2, 1);
            this.add(statusLabel, 0, 3, 2, 1);
            this.add(cancelButton, 0, 4, 2, 1);

        } else {
            this.getChildren().removeAll(progressIndicator, statusLabel, cancelButton);
            driveSelectChoiceBox.setDisable(false);

            this.add(wipeButton, 0, 2, 2, 1);
        }
    }

    ChoiceBox<String> getDriveSelectChoiceBox() {
        return driveSelectChoiceBox;
    }

    Button getCancelButton() {
        return cancelButton;
    }

    Button getWipeButton() {
        return wipeButton;
    }

    ProgressIndicator getProgressIndicator() {
        return progressIndicator;
    }

    Label getStatusLabel() {
        return statusLabel;
    }
}
