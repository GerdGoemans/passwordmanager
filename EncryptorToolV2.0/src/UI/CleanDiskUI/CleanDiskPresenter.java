package UI.CleanDiskUI;

import UI.UIConstants;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import model.WipeFreeSpaceTask;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static UI.UIConstants.windowNotification;

public class CleanDiskPresenter {

    private Stage currentStage;
    private Scene previousScene;
    private Scene currentScene;
    private CleanDiskView cleanDiskView;
    private List<File> driveList;
    private WipeFreeSpaceTask wipeFreeSpaceTask;
    private Thread wipeFreeSpakeThread;

    public CleanDiskPresenter(Stage currentStage, CleanDiskView cleanDiskView, Scene previousScene) {
        this.currentStage = currentStage;
        this.cleanDiskView = cleanDiskView;
        this.previousScene = previousScene;
        currentScene = currentStage.getScene();

        updateView();
        addEventHandlers();
    }

    private void addEventHandlers() {
        cleanDiskView.getCancelButton().setOnAction(event -> {
            if (wipeFreeSpaceTask == null) return;
            wipeFreeSpaceTask.haltTask();
        });
        cleanDiskView.getWipeButton().setOnAction(event -> {

            driveList = Arrays.asList(File.listRoots());

            File disk = null;
            for (File file : driveList) {
                String value = cleanDiskView.getDriveSelectChoiceBox().getValue();
                if (value.equals(file.toString())) {
                    disk = file;
                }
            }
            if (disk == null) return;

            wipeFreeSpaceTask = new WipeFreeSpaceTask(disk);

            cleanDiskView.showProgressIndicator(true);
            if (!currentStage.isMaximized()) {
                currentStage.sizeToScene();
            }

            cleanDiskView.getProgressIndicator().progressProperty().unbind();
            cleanDiskView.getProgressIndicator().progressProperty().bind(wipeFreeSpaceTask.progressProperty());
            cleanDiskView.getStatusLabel().textProperty().unbind();
            cleanDiskView.getStatusLabel().textProperty().bind(wipeFreeSpaceTask.messageProperty());

            wipeFreeSpaceTask.addEventFilter(WorkerStateEvent.WORKER_STATE_FAILED, event1 -> {
                defaultWokerStateEventCode();
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Error", "An error occured while cleaning disk", wipeFreeSpaceTask.getException().getMessage());
                alert.show();
            });
            wipeFreeSpaceTask.addEventFilter(WorkerStateEvent.WORKER_STATE_SUCCEEDED, event1 -> defaultWokerStateEventCode());
            wipeFreeSpaceTask.addEventFilter(WorkerStateEvent.WORKER_STATE_CANCELLED, event1 -> defaultWokerStateEventCode());

            wipeFreeSpakeThread = new Thread(wipeFreeSpaceTask);
            wipeFreeSpakeThread.start();
        });
        cleanDiskView.getDriveSelectChoiceBox().addEventFilter(ComboBox.ON_SHOWN, event -> updateView());
    }

    private void defaultWokerStateEventCode() {
        cleanDiskView.getProgressIndicator().progressProperty().unbind();
        cleanDiskView.getStatusLabel().textProperty().unbind();
        cleanDiskView.showProgressIndicator(false);
        wipeFreeSpaceTask = null;
        if (currentStage.getScene() == currentScene) {
            windowNotification(currentStage);
        }
    }

    private void updateView() {
        driveList = Arrays.asList(File.listRoots());
        List<String> list = cleanDiskView.getDriveSelectChoiceBox().getItems();
        driveList.forEach(file -> {
            if (!list.contains(file.toString())) {
                cleanDiskView.getDriveSelectChoiceBox().getItems().add(file.toString());
            }
        });
    }
}
