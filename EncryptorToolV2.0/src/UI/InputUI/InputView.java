package UI.InputUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

public class InputView extends GridPane {

    private Label label;
    private InputViewType inputViewType;
    private PasswordField passwordField;
    private TextField textField;
    private Button okButton;
    private Button cancelButton;

    public InputView(String header, InputViewType inputViewType) {
        this.inputViewType = inputViewType;
        initializeNodes(header);
        layoutNodes();
    }

    private void initializeNodes(String labelText) {
        label = new Label(labelText);
        if (inputViewType == InputViewType.password) {
            passwordField = new PasswordField();
            passwordField.setPromptText("Current password");
        } else if (inputViewType == InputViewType.datastrage) {
            textField = new TextField();
            textField.setPromptText("Name");
        } else if (inputViewType == InputViewType.mail) {
            textField = new TextField();
            textField.setPromptText("Mail");
        }

        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
    }

    private void layoutNodes() {


        ColumnConstraints columnConstraint1 = new ColumnConstraints();
        columnConstraint1.setPercentWidth(50);
        this.getColumnConstraints().addAll(columnConstraint1, columnConstraint1);

        this.add(label, 0, 0, 2, 1);
        GridPane.setMargin(label, new Insets(4));
        GridPane.setHalignment(label, HPos.CENTER);

        if (inputViewType == InputViewType.password) {
            this.add(passwordField, 0, 1, 2, 1);
            passwordField.setPrefWidth(30);
            GridPane.setMargin(passwordField, new Insets(4));
        } else if (inputViewType == InputViewType.datastrage || inputViewType == InputViewType.mail) {
            this.add(textField, 0, 1, 2, 1);
            textField.setPrefWidth(30);
            GridPane.setMargin(textField, new Insets(4));
        }

        this.add(okButton, 0, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        GridPane.setMargin(okButton, new Insets(4));
        okButton.setPrefWidth(70);

        this.add(cancelButton, 1, 2);
        GridPane.setHalignment(cancelButton, HPos.LEFT);
        GridPane.setMargin(cancelButton, new Insets(4));
        cancelButton.setPrefWidth(70);

        this.setHgap(4);
        this.setVgap(4);
        this.setPadding(new Insets(8));
        this.setAlignment(Pos.CENTER);
    }

    PasswordField getPasswordField() {
        return passwordField;
    }

    Button getOkButton() {
        return okButton;
    }

    Button getCancelButton() {
        return cancelButton;
    }

    public InputViewType getInputViewType() {
        return inputViewType;
    }

    TextField getTextField() {
        return textField;
    }
}
