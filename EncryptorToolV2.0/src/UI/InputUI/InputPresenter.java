package UI.InputUI;


import UI.UIConstants;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import model.DataStorage;
import model.MailList;

public class InputPresenter {

    private InputView inputView;
    private Stage stage;
    private InputViewType inputViewType;

    public InputPresenter(UI.InputUI.InputView inputView, Stage stage) {
        this.inputView = inputView;
        this.stage = stage;
        this.inputViewType = inputView.getInputViewType();

        addEventhandlers();
    }

    private void addEventhandlers() {

        inputView.getOkButton().setOnAction(event -> okButtonEvent());
        inputView.getCancelButton().setOnAction(event -> stage.close());

        KeyCodeCombination ctrlBackspace = new KeyCodeCombination(KeyCode.BACK_SPACE, KeyCombination.CONTROL_DOWN);
        TextField textField = inputView.getTextField();
        if (textField != null) {
            textField.setOnKeyPressed(ke -> {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    okButtonEvent();
                }

                if (ctrlBackspace.match(ke)) {
                    inputView.getTextField().setText("");
                }
            });
        }

        PasswordField passwordField = inputView.getPasswordField();
        if (passwordField != null) {
            passwordField.setOnKeyPressed(ke -> {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    okButtonEvent();
                }

                if (ctrlBackspace.match(ke)) {
                    inputView.getPasswordField().setText("");
                }
            });
        }

    }

    private void okButtonEvent() {
        if (inputViewType == InputViewType.password) {
            UIConstants.setMasterPassword(inputView.getPasswordField().getText());
        } else if (inputViewType == InputViewType.datastrage) {
            try {
                new DataStorage(inputView.getTextField().getText(), "", false);
            } catch (Exception e) {
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Error", "An exception occured", e.getMessage());
                alert.show();
            }
        } else if (inputViewType == InputViewType.mail) {
            new MailList(inputView.getTextField().getText());
        }

        stage.close();
    }
}
