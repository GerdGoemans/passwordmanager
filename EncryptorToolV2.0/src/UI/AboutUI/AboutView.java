package UI.AboutUI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class AboutView extends BorderPane {

    private Button returnButton;
    private Label title;
    private TextArea mainContent;

    public AboutView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {

        title = new Label("About this program");

        mainContent = new TextArea("Copyright (c) 2018 Stanhope\n" +
                "For more information visit gitlab.com/stanhope\n" +
                "\n" +
                " Permission is hereby granted, free of charge, to any person" +
                " obtaining a copy of this software and associated documentation" +
                " files (the \"Software\"), to deal in the Software without" +
                " restriction, including without limitation the rights to use," +
                " copy, modify, merge, publish, distribute, sublicense, and/or sell" +
                " copies of the Software, and to permit persons to whom the" +
                " Software is furnished to do so, subject to the following" +
                " conditions:\n" +
                "\n" +
                " The above copyright notice and this permission notice shall be" +
                " included in all copies or substantial portions of the Software.\n" +
                "\n" +
                " THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,"+
                " EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES" +
                " OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND" +
                " NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT" +
                " HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY," +
                " WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING" +
                " FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR" +
                " OTHER DEALINGS IN THE SOFTWARE.");

        mainContent.setEditable(false);
        mainContent.setWrapText(true);
        mainContent.setId("CenterText");

        returnButton = new Button("Return");
    }

    private void layoutNodes() {
        this.setPadding(new Insets(8));

        this.setTop(title);
        this.setCenter(mainContent);
        this.setBottom(returnButton);

        mainContent.setPrefWidth(Double.MAX_VALUE);
        mainContent.setPrefHeight(Double.MAX_VALUE);

        BorderPane.setAlignment(title, Pos.CENTER);
        BorderPane.setMargin(mainContent, new Insets(12, 12, 12, 12));
        BorderPane.setAlignment(returnButton, Pos.BOTTOM_RIGHT);

    }

    Button getReturnButton() {
        return returnButton;
    }
}
