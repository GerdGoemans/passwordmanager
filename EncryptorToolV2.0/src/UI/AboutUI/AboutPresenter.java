package UI.AboutUI;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class AboutPresenter {

    private Stage primaryStage;
    private AboutView aboutView;

    public AboutPresenter(Stage primaryStage, AboutView aboutView, Scene aboutScene) {
        this.primaryStage = primaryStage;
        this.aboutView = aboutView;

        aboutScene.getStylesheets().add("CSS/General.css");
        addEventHandlers();
    }

    private void addEventHandlers() {
        aboutView.getReturnButton().setOnAction(event -> primaryStage.close());
    }
}
