package UI.HaveIBeenPwnedUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class PwnedView extends GridPane {
    private TextField passwordField;
    private Button passwordButton;
    private TextArea resultArea;

    private TextField emailField;
    private Button emailButton;

    public PwnedView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        passwordField = new TextField();
        passwordField.setPromptText("Enter password");
        passwordButton = new Button("Go");
        resultArea = new TextArea();
        resultArea.setEditable(false);
        resultArea.setWrapText(true);

        emailField = new TextField();
        emailField.setPromptText("Enter email");
        emailButton = new Button("Go");

    }

    private void layoutNodes() {
        this.add(new Label("Enter password: "), 0, 0);
        this.add(passwordField, 1, 0);
        this.add(new Label("Enter email: "), 2, 0);
        this.add(emailField, 3, 0);

        this.add(passwordButton, 0, 1, 2, 1);
        this.add(emailButton, 2, 1, 2, 1);

        this.add(resultArea, 0, 2, 4, 1);

        GridPane.setHalignment(passwordButton, HPos.CENTER);
        GridPane.setHalignment(emailButton, HPos.CENTER);
        GridPane.setVgrow(resultArea, Priority.ALWAYS);

        ColumnConstraints col20 = new ColumnConstraints();
        col20.setPercentWidth(15);
        ColumnConstraints col30 = new ColumnConstraints();
        col30.setPercentWidth(35);
        this.getColumnConstraints().addAll(col20, col30, col20, col30);

        this.setPadding(new Insets(4, 8, 4, 8));
        this.setHgap(2);
        this.setVgap(4);
    }

    TextField getPasswordField() {
        return passwordField;
    }

    Button getPasswordButton() {
        return passwordButton;
    }

    TextArea getResultArea() {
        return resultArea;
    }

    TextField getEmailField() {
        return emailField;
    }

    Button getEmailButton() {
        return emailButton;
    }
}
