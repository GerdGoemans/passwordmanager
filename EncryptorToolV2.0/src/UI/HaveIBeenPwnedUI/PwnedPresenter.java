package UI.HaveIBeenPwnedUI;

import UI.TabPaneUI.TabPaneView;
import UI.UIConstants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class PwnedPresenter {
    private PwnedView pwnedView;

    public PwnedPresenter(PwnedView pwnedView) {
        this.pwnedView = pwnedView;
        addEventHandlers();
    }

    private void addEventHandlers() {
        pwnedView.getPasswordButton().setOnAction(event -> {
            TabPaneView.getStatusLabel().setText("Fetching data for password ...");
            passwordButtonEvent();
        });

        pwnedView.getPasswordField().setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                TabPaneView.getStatusLabel().setText("Fetching data for password ...");
                passwordButtonEvent();
            }
        });

        pwnedView.getEmailButton().setOnAction(event -> {
            TabPaneView.getStatusLabel().setText("Fetching data for email ...");
            emailButtonEvent();
        });

        pwnedView.getEmailField().setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                TabPaneView.getStatusLabel().setText("Fetching data for email ...");
                emailButtonEvent();
            }
        });
    }

    private void emailButtonEvent() {

        try {
            String mail = pwnedView.getEmailField().getText();
            if (mail.length() == 0)
                throw new IllegalArgumentException("E-mail length was 0, please ensure that you've entered a e-mail in the field.");

            System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0");
            URL url = new URL("https://haveibeenpwned.com/unifiedsearch/" + URLEncoder.encode(mail, "UTF-8"));
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0");
            con.setRequestMethod("GET");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);

            int status = con.getResponseCode();
            if (status == 404) {
                pwnedView.getResultArea().setText("You are safe!  No breaches were detected.");
                return;
            }
            if (status > 299) {
                pwnedView.getResultArea().setText("Error: " + String.valueOf(status));
                //return;
            }

            pwnedView.getResultArea().setText("");

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader(con.getInputStream()));
            JsonObject rootobj = root.getAsJsonObject();
            TextArea textArea = pwnedView.getResultArea();
            int breaches = rootobj.get("Breaches").getAsJsonArray().size();
            if (breaches > 0) {
                textArea.setText("That email has been pwned " + breaches + " times.");
                rootobj.get("Breaches").getAsJsonArray().forEach(jsonElement -> {
                    JsonObject obj = jsonElement.getAsJsonObject();
                    textArea.appendText("\n\n");
                    textArea.appendText("Site that was pwned: " + obj.get("Name") + " (site: " + obj.get("Domain") + " )");
                    textArea.appendText("\n");
                    textArea.appendText(obj.get("Description").toString());
                    textArea.appendText("\n\n");
                    textArea.appendText("Data affected: ");
                    obj.get("DataClasses").getAsJsonArray().forEach(el -> textArea.appendText(el.toString() + ", "));
                    textArea.appendText("\n");
                    textArea.appendText("Is verified? " + obj.get("IsVerified"));
                    textArea.appendText("\n");
                    textArea.appendText("Date of breach: " + obj.get("BreachDate"));
                });
            } else {
                pwnedView.getResultArea().setText("You are safe!  No breaches were detected.");
            }
            TabPaneView.getStatusLabel().setText("Done fetching data for email");
        } catch (Exception e) {
            TabPaneView.getStatusLabel().setText("Error while fetching data for email");
            Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Something went wrong", "Something went wrong", e.getMessage());
            alert.show();
        }
    }

    private void passwordButtonEvent() {
        try {
            String pass = pwnedView.getPasswordField().getText();
            if (pass.length() == 0)
                throw new IllegalArgumentException("Password length was 0, please ensure that you've entered a password in the field.");
            String sha1 = UIConstants.getEnDecryptor().sha1Hash(pass);
            String first5Sha1 = sha1.substring(0, 5);

            System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0");
            URL url = new URL("https://api.pwnedpasswords.com/range/" + first5Sha1);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0");
            con.setRequestMethod("GET");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);

            int status = con.getResponseCode();
            pwnedView.getResultArea().setText("");

            if (status < 299) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                boolean pwned = false;
                while ((inputLine = in.readLine()) != null) {
                    String[] splitString = inputLine.split(":");
                    if ((first5Sha1 + splitString[0]).equalsIgnoreCase(sha1)) {
                        pwned = true;
                        pwnedView.getResultArea().appendText("Your password has been pwned!");
                        pwnedView.getResultArea().appendText("\n");
                        pwnedView.getResultArea().appendText("It was encountered " + splitString[1] + " times in the database of haveibeenpwned.com");
                    }
                }
                in.close();

                if (!pwned) {
                    pwnedView.getResultArea().appendText("Your password does not appear in the database of HaveIBeenPwned.com");
                }
            } else {
                pwnedView.getResultArea().setText("Error: " + String.valueOf(status));
            }
            TabPaneView.getStatusLabel().setText("Done fetching data for password");
        } catch (Exception e) {
            Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Something went wrong", "Something went wrong", e.getMessage());
            alert.show();
            TabPaneView.getStatusLabel().setText("Error while fetching data for password");
        }
    }
}
