package UI.PasswordManagerUI;

import UI.InputUI.InputPresenter;
import UI.InputUI.InputView;
import UI.InputUI.InputViewType;
import UI.SetupMasterPasswordUI.SetupMasterPWPresenter;
import UI.SetupMasterPasswordUI.SetupMasterPWView;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.DataStorage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static UI.UIConstants.*;
import static model.DataStorage.getDataStorageList;
import static model.MasterPasswordManager.loadMasterPassword;
import static model.MasterPasswordManager.masterPasswordExists;

public class PasswordManagerPresenter {

    private PasswordManagerView passwordManagerView;
    private DataStorage currentDataStorage;
    private boolean isUserChange = true;
    private boolean showingEncrypted = false;

    public PasswordManagerPresenter(PasswordManagerView passwordManagerView, Stage stage) throws IOException {
        this.passwordManagerView = passwordManagerView;

        //setup master password
        if (masterPasswordExists()) {
            try {
                byte[][] returnValue = loadMasterPassword();
                setMasterPasswordHash(returnValue[0]);
                setMasterPasswordSalt(returnValue[1]);
            } catch (IOException e) {
                Alert alert = alert(Alert.AlertType.ERROR, "Error", "An error occurred while setting up the master password", e.getMessage());
                alert.show();
                stage.close();
            }
        } else {
            Stage secondaryStage = new Stage();
            SetupMasterPWView setupMasterPWView = new SetupMasterPWView();
            Scene masterPWScene = new Scene(setupMasterPWView);
            masterPWScene.getStylesheets().add("CSS/General.css");
            new SetupMasterPWPresenter(stage, setupMasterPWView, getEnDecryptor());

            secondaryStage.setScene(masterPWScene);
            secondaryStage.setWidth(300);
            secondaryStage.setHeight(200);
            secondaryStage.setTitle("New preset name");
            secondaryStage.showAndWait();
        }

        //get previous save data
        try {
            DataStorage.loadDataStorages();
        } catch (Exception e) {
            Alert alert = alert(Alert.AlertType.ERROR, "Error", "An error occurred while loading previous saves.", e.getMessage());
            alert.show();
            e.printStackTrace();
        }

        if (getDataStorageList().size() > 0) {
            this.currentDataStorage = getDataStorageList().get(0);
        } else {
            new DataStorage("Misc", "", false);
            this.currentDataStorage = getDataStorageList().get(0);
        }
        if (currentDataStorage.getContent() != null) {
            showingEncrypted = true;
        }

        addEventHandlers();
        updateView(null);
    }

    private void updateView(List<DataStorage> dataStorageList) {
        VBox toggleButtonVBox = passwordManagerView.getToggleVBox();
        toggleButtonVBox.getChildren().clear();

        if (dataStorageList == null) {
            dataStorageList = getDataStorageList();
        }

        List<RadioButton> toggleButtonList = new ArrayList<>();
        for (DataStorage dataStorage : dataStorageList) {
            RadioButton newButton = new RadioButton(dataStorage.getName());
            toggleButtonList.add(newButton);
        }
        toggleButtonList.sort((o1, o2) -> o1.getText().compareToIgnoreCase(o2.getText()));
        toggleButtonList.forEach(radioButton -> {
            radioButton.setToggleGroup(passwordManagerView.getToggleGroup());
            if (radioButton.getText().equals(currentDataStorage.getName())) {
                this.passwordManagerView.getToggleGroup().selectToggle(radioButton);
            }
        });

        toggleButtonVBox.getChildren().addAll(toggleButtonList);

        this.passwordManagerView.getToggleButton().setText("Toggle showing");

        isUserChange = false;
        passwordManagerView.getPresetName().setText(currentDataStorage.getName());

        if (showingEncrypted) {
            passwordManagerView.getToggleButton().setText("Show decrypted");
            passwordManagerView.getEnDeCryptButton().setText("Decrypt");
            passwordManagerView.getTextArea().setText(currentDataStorage.getContent());
        } else {
            passwordManagerView.getToggleButton().setText("Show encrypted");
            passwordManagerView.getEnDeCryptButton().setText("Encrypt");
            passwordManagerView.getTextArea().setText(currentDataStorage.getUnEncryptedContent());
        }
    }

    private void addEventHandlers() {
        /*Change listeners*/
        this.passwordManagerView.getTextArea().textProperty().addListener((observable, oldValue, newValue) -> {
            if (isUserChange) {
                /*Only do something if it's a user change*/
                if (showingEncrypted) {
                    currentDataStorage.setContent(newValue);
                } else {
                    currentDataStorage.setUnEncryptedContent(newValue);
                }
            } else {
                /*Program change got detected, resetting so the next change is picked up*/
                isUserChange = true;
            }
        });
        this.passwordManagerView.getToggleGroup().selectedToggleProperty().addListener((obs, oldToggle, newToggle) -> {
            String value = obs.getValue().toString();
            value = value.substring(value.indexOf("'") + 1, value.lastIndexOf("'"));
            if (currentDataStorage.getName().equalsIgnoreCase(value)) return;
            for (DataStorage dataStorage : getDataStorageList()) {
                if (dataStorage.getName().equalsIgnoreCase(value)) {
                    currentDataStorage = dataStorage;
                    updateView(null);
                    return;
                }
            }
        });

        /*Actual UI events*/
        this.passwordManagerView.getEnDeCryptButton().setOnAction(event -> {
            if (showingEncrypted) {
                try {
                    if (!askUserForPassword()) return;
                    getEnDecryptor().decryptDataStorage(getMasterPassword(), currentDataStorage);
                    showingEncrypted = !showingEncrypted;
                    updateView(null);
                } catch (Exception e) {
                    Alert alert1 = alert(Alert.AlertType.ERROR, "ERROR", "Something went wrong while decrypting:", e.getMessage());
                    alert1.show();
                } finally {
                    setMasterPassword("");
                }
            } else {
                try {
                    if (!askUserForPassword()) return;
                    getEnDecryptor().encryptDataStorage(getMasterPassword(), currentDataStorage);
                    showingEncrypted = !showingEncrypted;
                    updateView(null);
                } catch (Exception e) {
                    Alert alert1 = alert(Alert.AlertType.ERROR, "ERROR", "Something went wrong while decrypting:", e.getMessage());
                    alert1.show();
                } finally {
                    setMasterPassword("");
                }
            }
            updateView(null);
        });
        this.passwordManagerView.getToggleButton().setOnAction(event -> {
            showingEncrypted = !showingEncrypted;
            updateView(null);
        });
        this.passwordManagerView.getAddButton().setOnAction(event -> {
            Stage secondaryStage = new Stage();
            InputView inputView = new InputView("New preset name:", InputViewType.datastrage);
            new InputPresenter(inputView, secondaryStage);
            Scene inputScene = new Scene(inputView);
            secondaryStage.setScene(inputScene);
            inputScene.getStylesheets().add("CSS/General.css");

            setupStage(secondaryStage);
            secondaryStage.showAndWait();
            currentDataStorage = getDataStorageList().get(getDataStorageList().size() - 1);
            updateView(null);
        });
        this.passwordManagerView.getRemoveButton().setOnAction(event -> {
            Alert confirmAlert = alert(Alert.AlertType.CONFIRMATION, "Confirm remove?", "Are you sure you want to remove " + currentDataStorage.getName() + "?", "This action cannot be undone.");
            confirmAlert.getButtonTypes().clear();
            ButtonType yes = new ButtonType("Yes");
            ButtonType no = new ButtonType("No");
            confirmAlert.getButtonTypes().addAll(yes, no);
            confirmAlert.showAndWait();
            if (confirmAlert.getResult() == null || confirmAlert.getResult().equals(no)) {
                event.consume();
                return;
            }

            DataStorage.removeDataStorage(currentDataStorage);
            currentDataStorage = getDataStorageList().get(0);
            updateView(null);
        });
        this.passwordManagerView.getSearchField().textProperty().addListener((observable, oldValue, newValue) -> {
            List<DataStorage> dataStorageList = getDataStorageList();
            List<DataStorage> filteredList = new ArrayList<>();
            if (newValue == null || newValue.equals("")) {
                updateView(getDataStorageList());
            } else {
                dataStorageList.forEach(dataStorage -> {
                    if (dataStorage.getName().toLowerCase().contains(newValue.toLowerCase())) {
                        filteredList.add(dataStorage);
                    }
                });
                updateView(filteredList);
            }
        });
    }
}

