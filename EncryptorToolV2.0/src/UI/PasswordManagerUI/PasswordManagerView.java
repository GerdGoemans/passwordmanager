package UI.PasswordManagerUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.ArrayList;

public class PasswordManagerView extends BorderPane {

    private Stage stage;

    /*main (center)*/
    private SplitPane centerPane;

    /*center left*/
    private GridPane centerLeftPane;
    private Button addButton;
    private Button removeButton;
    private HBox optionsHBox;
    private ToggleGroup toggleGroup;
    private ArrayList<RadioButton> toggleButtonList;
    private VBox toggleVBox;
    private TextField searchField;
    private ImageView searchImage;

    /*center right*/
    private GridPane centerRightPane;
    private TextArea textArea;
    private Button toggleButton;
    private Button enDeCryptButton;
    private Label presetName;

    public PasswordManagerView(Stage stage) {
        this.stage = stage;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        /*Center*/
        centerPane = new SplitPane();
        centerLeftPane = new GridPane();
        centerRightPane = new GridPane();

        textArea = new TextArea("");
        textArea.setWrapText(true);
        toggleButton = new Button("Show encrypted");
        enDeCryptButton = new Button("Decrypt");

        addButton = new Button("Add");
        removeButton = new Button("Remove");
        optionsHBox = new HBox(addButton, removeButton);
        searchField = new TextField();
        searchField.setPromptText("Search ...");
        searchImage = new ImageView(new Image("/images/search.png"));

        toggleGroup = new ToggleGroup();
        toggleButtonList = new ArrayList<>();
        toggleVBox = new VBox();

        presetName = new Label("");
    }

    private void layoutNodes() {
        /*Center left*/
        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(100);
        centerLeftPane.getColumnConstraints().add(col);

        centerLeftPane.setPadding(new Insets(4, 8, 4, 8));
        centerLeftPane.setHgap(2);
        centerLeftPane.setVgap(4);
        centerLeftPane.add(new Label(""), 0, 0);

        optionsHBox.setSpacing(4);
        centerLeftPane.add(optionsHBox, 0, 1);

        ScrollPane scrollPane = new ScrollPane(toggleVBox);
        centerLeftPane.add(scrollPane, 0, 2);
        GridPane.setVgrow(scrollPane, Priority.ALWAYS);

        HBox searchHBox = new HBox();
        searchHBox.setSpacing(2);
        searchHBox.getChildren().addAll(searchImage, searchField);
        centerLeftPane.add(searchHBox, 0, 3);

        searchImage.setPreserveRatio(true);
        searchImage.setFitHeight(25);
        searchField.setPrefWidth(200);

        centerLeftPane.setMinWidth(150);

        /*center right*/
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(50);
        centerRightPane.getColumnConstraints().addAll(column, column);

        centerRightPane.setPadding(new Insets(4, 8, 4, 8));
        centerRightPane.setHgap(2);
        centerRightPane.setVgap(4);
        centerRightPane.add(presetName, 0, 0);
        HBox buttonHbox = new HBox(enDeCryptButton, toggleButton);
        centerRightPane.add(buttonHbox, 1, 0);
        centerRightPane.add(textArea, 0, 1, 2, 1);

        buttonHbox.setSpacing(2);
        buttonHbox.setAlignment(Pos.BASELINE_RIGHT);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHalignment(buttonHbox, HPos.RIGHT);

        centerRightPane.setMinWidth(200);

        /*center*/
        SplitPane.setResizableWithParent(centerLeftPane, false);
        SplitPane.setResizableWithParent(centerRightPane, true);
        centerPane.getItems().addAll(centerLeftPane, centerRightPane);
        centerPane.setDividerPositions(centerLeftPane.getWidth() / stage.getWidth());

        this.setCenter(this.centerPane);
    }


    Button getAddButton() {
        return addButton;
    }

    Button getRemoveButton() {
        return removeButton;
    }

    ToggleGroup getToggleGroup() {
        return toggleGroup;
    }

    ArrayList<RadioButton> getToggleButtonList() {
        return toggleButtonList;
    }

    VBox getToggleVBox() {
        return toggleVBox;
    }

    TextArea getTextArea() {
        return textArea;
    }

    Button getToggleButton() {
        return toggleButton;
    }

    Label getPresetName() {
        return presetName;
    }

    TextField getSearchField() {
        return searchField;
    }

    Button getEnDeCryptButton() {
        return enDeCryptButton;
    }
}
