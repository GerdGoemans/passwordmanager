package UI.SetupMasterPasswordUI;

import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.EnDecryptor;

import java.nio.charset.StandardCharsets;

import static UI.UIConstants.alert;
import static model.MasterPasswordManager.saveMasterPassword;


public class SetupMasterPWPresenter {

    private Stage currentStage;
    private SetupMasterPWView setupMasterPWView;
    private EnDecryptor encryptorDecryptor;

    public SetupMasterPWPresenter(Stage currentStage, SetupMasterPWView setupMasterPWView, EnDecryptor encryptorDecryptor) {
        this.currentStage = currentStage;
        this.setupMasterPWView = setupMasterPWView;
        this.encryptorDecryptor = encryptorDecryptor;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        setupMasterPWView.getPasswordField1().setOnKeyReleased(event -> updateView());
        setupMasterPWView.getPasswordField2().setOnKeyReleased(event -> updateView());

        setupMasterPWView.getCancelButton().setOnAction(event -> currentStage.close());
        setupMasterPWView.getConfirmButton().setOnAction(event -> {
            try {
                byte[] salt = EnDecryptor.generateSalt();
                byte[] password = setupMasterPWView.getPasswordField2().getText().getBytes(StandardCharsets.UTF_8);

                byte[] hashedPassword = encryptorDecryptor.hashMasterPassword(password, salt);
                saveMasterPassword(hashedPassword, salt);

                currentStage.close();
            } catch (Exception e) {
                Alert alert = alert(Alert.AlertType.ERROR, "Error", "Something went wrong", e.getMessage());
                alert.show();
                e.printStackTrace();
            }
        });

    }

    private void updateView() {
        String pw1 = setupMasterPWView.getPasswordField1().getText();
        if (pw1 == null || pw1.equals("")) {
            setupMasterPWView.getPasswordOKLabel().setText("");
            return;
        }

        String pw2 = setupMasterPWView.getPasswordField2().getText();

        if (pw2 == null || pw2.equals("")) {
            if (pw1.length() < 10) {
                setupMasterPWView.getPasswordOKLabel().setText("Password is too short");
                setupMasterPWView.getPasswordOKLabel().setTextFill(Color.RED);
            } else {
                setupMasterPWView.getPasswordOKLabel().setText("");
                setupMasterPWView.getPasswordOKLabel().setTextFill(Color.LIGHTGREEN);
            }
        } else {
            if (pw1.equals(pw2)) {
                setupMasterPWView.getPasswordOKLabel().setText("Password OK");
                setupMasterPWView.getPasswordOKLabel().setTextFill(Color.LIGHTGREEN);
                setupMasterPWView.getConfirmButton().setDisable(false);
            } else {
                setupMasterPWView.getPasswordOKLabel().setText("Passwords do not match");
                setupMasterPWView.getPasswordOKLabel().setTextFill(Color.RED);
            }
        }

    }

}
