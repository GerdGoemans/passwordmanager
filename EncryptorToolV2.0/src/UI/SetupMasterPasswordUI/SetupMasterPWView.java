package UI.SetupMasterPasswordUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;


public class SetupMasterPWView extends GridPane {

    private PasswordField passwordField1;
    private PasswordField passwordField2;

    private Button confirmButton;
    private Button cancelButton;

    private Label passwordOKLabel;
    private Label explenationLabel;

    public SetupMasterPWView() {

        setupNodes();
        layoutNodes();
    }

    private void setupNodes() {
        passwordField1 = new PasswordField();
        passwordField1.setPromptText("Enter password");
        passwordField2 = new PasswordField();
        passwordField2.setPromptText("Confirm password");

        confirmButton = new Button("Confirm password");
        confirmButton.setDisable(true);
        cancelButton = new Button("Cancel");

        passwordOKLabel = new Label();
        passwordOKLabel.setPrefWidth(Double.MAX_VALUE);
        explenationLabel = new Label("Set up new master password:");
    }

    private void layoutNodes() {
        this.add(explenationLabel, 0, 0, 2, 1);
        this.add(passwordField1, 0, 1, 2, 1);
        this.add(passwordField2, 0, 2, 2, 1);
        this.add(passwordOKLabel, 0, 3, 2, 1);

        GridPane.setHalignment(passwordOKLabel, HPos.CENTER);
        GridPane.setHalignment(explenationLabel, HPos.CENTER);

        this.add(cancelButton, 0, 4);
        this.add(confirmButton, 1, 4);
        GridPane.setHalignment(confirmButton, HPos.CENTER);
        GridPane.setHalignment(cancelButton, HPos.CENTER);

        ColumnConstraints columnConstraint1 = new ColumnConstraints();
        columnConstraint1.setPercentWidth(50);
        this.getColumnConstraints().addAll(columnConstraint1, columnConstraint1);

        this.setPadding(new Insets(4, 8, 4, 8));
        this.setHgap(4);
        this.setVgap(4);
        this.setAlignment(Pos.CENTER);
    }

    PasswordField getPasswordField1() {
        return passwordField1;
    }

    PasswordField getPasswordField2() {
        return passwordField2;
    }

    Button getConfirmButton() {
        return confirmButton;
    }

    Button getCancelButton() {
        return cancelButton;
    }

    Label getPasswordOKLabel() {
        return passwordOKLabel;
    }
}
