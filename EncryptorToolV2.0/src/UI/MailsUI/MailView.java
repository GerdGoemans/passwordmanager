package UI.MailsUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import model.MailList;

public class MailView extends GridPane {

    private ListView<MailList> mailListListView;
    private Button addButton;
    private Button removeButton;
    private Button checkPwnsButton;

    public MailView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        mailListListView = new ListView<>();
        addButton = new Button("Add");
        removeButton = new Button("Remove");
        checkPwnsButton = new Button("Check for pwns");
    }

    private void layoutNodes() {
        this.setPadding(new Insets(4, 8, 4, 8));
        this.setHgap(4);
        this.setVgap(4);

        this.add(mailListListView, 0, 0, 3, 1);
        this.add(removeButton, 0, 1);
        this.add(checkPwnsButton, 1, 1);
        this.add(addButton, 2, 1);

        GridPane.setHalignment(checkPwnsButton, HPos.CENTER);
        GridPane.setHalignment(addButton, HPos.RIGHT);

        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(33.33);
        this.getColumnConstraints().addAll(col, col, col);
    }

    ListView<MailList> getMailListListView() {
        return mailListListView;
    }

    Button getAddButton() {
        return addButton;
    }

    Button getRemoveButton() {
        return removeButton;
    }

    Button getCheckPwnsButton() {
        return checkPwnsButton;
    }
}
