package UI.MailsUI;

import UI.InputUI.InputPresenter;
import UI.InputUI.InputView;
import UI.InputUI.InputViewType;
import UI.UIConstants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.MailList;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static UI.UIConstants.setupStage;

public class MailPresenter {

    private MailView mailView;
    private ListView<MailList> mailListListView;

    public MailPresenter(MailView mailView) {
        this.mailView = mailView;
        this.mailListListView = mailView.getMailListListView();
        try {
            MailList.loadMailList();
        } catch (IOException e) {
            Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Error", "Something went wrong while loading the mail list.", e.getMessage());
            alert.show();
        }
        addEventHanlders();
        updateView();
    }

    private void updateView() {
        mailListListView.setItems(FXCollections.observableArrayList(MailList.getMailList()));
    }

    private void addEventHanlders() {
        mailView.getAddButton().setOnAction(event -> {
            Stage secondaryStage = new Stage();
            InputView inputView = new InputView("Email to add", InputViewType.mail);
            new InputPresenter(inputView, secondaryStage);
            Scene inputScene = new Scene(inputView);
            secondaryStage.setScene(inputScene);
            inputScene.getStylesheets().add("CSS/General.css");

            setupStage(secondaryStage);
            secondaryStage.showAndWait();
            updateView();
            try {
                MailList.saveMailList();
            } catch (IOException e) {
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Error", "Something went wrong while saving the mail list.", e.getMessage());
                alert.show();
            }
        });

        mailView.getRemoveButton().setOnAction(event -> {
            MailList currentSelected = mailListListView.getSelectionModel().getSelectedItem();
            List<MailList> mailList = new ArrayList<>(MailList.getMailList());

            Alert alert = UIConstants.alert(Alert.AlertType.CONFIRMATION, "Remove", "Are you sure you want to delete " + mailList.toString() + "?", "");
            alert.getButtonTypes().clear();
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");
            alert.getButtonTypes().addAll(noButton, yesButton);
            alert.showAndWait();

            if (!alert.getResult().equals(yesButton)) {
                event.consume();
                return;
            }

            mailList.remove(currentSelected);
            MailList.setMailList(mailList);
            updateView();

            try {
                MailList.saveMailList();
            } catch (IOException e) {
                Alert alert2 = UIConstants.alert(Alert.AlertType.ERROR, "Error", "Something went wrong while saving the mail list.", e.getMessage());
                alert2.show();
            }
        });

        mailView.getCheckPwnsButton().setOnAction(event -> {
            List<MailList> mailLists = new ArrayList<>(MailList.getMailList());
            List<MailList> pwnedMails = new ArrayList<>();
            List<MailList> errorList = new ArrayList<>();
            mailLists.forEach(mailList -> {
                try {
                    URL url = new URL("https://haveibeenpwned.com/unifiedsearch/" + URLEncoder.encode(mailList.getMail(), "UTF-8"));
                    HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0");
                    con.setConnectTimeout(5000);
                    con.setReadTimeout(5000);

                    int status = con.getResponseCode();
                    if (status > 299) {
                        if (status != 404) errorList.add(mailList);
                    } else {
                        JsonParser jp = new JsonParser();
                        JsonElement root = jp.parse(new InputStreamReader(con.getInputStream()));
                        JsonObject rootobj = root.getAsJsonObject();
                        int breaches = rootobj.get("Breaches").getAsJsonArray().size();
                        if (breaches > 0) {
                            pwnedMails.add(mailList);
                        }
                    }
                } catch (IOException e) {
                    errorList.add(mailList);
                }
            });

            if (errorList.size() > 0) {
                StringBuilder errorBuilder = new StringBuilder();
                for (int i = 0; i < errorList.size(); i++) {
                    errorBuilder.append(" ").append(errorList.get(i));
                    if (i != errorList.size()) {
                        errorBuilder.append(",");
                    }
                }
                Alert errorAlert = UIConstants.alert(Alert.AlertType.WARNING, "Warning", "Warning, the following emails were not checked", errorBuilder.toString());
                errorAlert.show();
            }

            if (pwnedMails.size() > 0) {
                StringBuilder pwnedMailsBuilder = new StringBuilder();
                for (int i = 0; i < pwnedMails.size(); i++) {
                    pwnedMailsBuilder.append(" ").append(pwnedMails.get(i));
                    if (i != pwnedMails.size()) {
                        pwnedMailsBuilder.append(",");
                    }
                }
                Alert pwnedAlert = UIConstants.alert(Alert.AlertType.INFORMATION, "You've been pwned", "You have been pwned, these mails were pwned:", pwnedMailsBuilder.toString());
                pwnedAlert.show();
            }
        });
    }
}
