package UI.RandomStringUI;

import UI.UIConstants;
import javafx.scene.control.Alert;

import java.security.SecureRandom;

public class RandomStringPresenter {

    private RandomStringView randomStringView;

    public RandomStringPresenter(RandomStringView randomStringView) {
        this.randomStringView = randomStringView;
        addEventHandlers();
    }

    private void addEventHandlers() {
        randomStringView.getMakeButton().setOnAction(event -> {
            SecureRandom secureRandom = new SecureRandom();
            char[] charToChoseFrom = randomStringView.getStringToChoseFromArea().getText().toCharArray();
            if (charToChoseFrom.length < 1) {
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Illegal input", "You must enter at least 1 character in the top textfield", "");
                alert.show();
                return;
            }
            int numberOfStrings = -1;
            try {
                numberOfStrings = Integer.parseInt(randomStringView.getNumberOfStringsField().getText());
                if (numberOfStrings == -1) throw new Exception("Something unknown went wrong");
            } catch (Exception e) {
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Illegal number of strings", "Something went wrong when getting the number of strings we needed to make", e.getMessage());
                alert.show();
            }
            int stringLength = -1;
            try {
                stringLength = Integer.parseInt(randomStringView.getStringLengthField().getText());
                if (numberOfStrings == -1) throw new Exception("Something unknown went wrong");
            } catch (Exception e) {
                Alert alert = UIConstants.alert(Alert.AlertType.ERROR, "Illegal string length", "Something went wrong when getting the length of the strings we needed to make", e.getMessage());
                alert.show();
            }

            for (int i = 0; i < numberOfStrings; i++) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < stringLength; j++) {
                    stringBuilder.append(charToChoseFrom[secureRandom.nextInt(charToChoseFrom.length)]);
                }
                randomStringView.getOutputArea().appendText(stringBuilder.toString());
                randomStringView.getOutputArea().appendText(System.getProperty("line.separator"));
            }
        });
    }

}
