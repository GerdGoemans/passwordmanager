package UI.RandomStringUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

public class RandomStringView extends GridPane {

    private TextArea stringToChoseFromArea;
    private TextField stringLengthField;
    private TextField numberOfStringsField;
    private Button makeButton;
    private TextArea outputArea;

    public RandomStringView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        stringToChoseFromArea = new TextArea();
        stringToChoseFromArea.setWrapText(true);
        stringLengthField = new TextField();
        numberOfStringsField = new TextField();
        makeButton = new Button("Make");
        outputArea = new TextArea();
        outputArea.setWrapText(true);
    }

    private void layoutNodes() {
        this.setMinWidth(200);
        this.setMinHeight(100);
        this.setPadding(new Insets(4, 8, 4, 8));
        this.setHgap(4);
        this.setVgap(4);

        this.add(new Label("Enter characters to chose from here"), 0, 0, 2, 1);
        this.add(stringToChoseFromArea, 0, 1, 2, 1);
        this.add(new Label("String length"), 0, 2);
        this.add(stringLengthField, 1, 2);
        this.add(new Label("Number of Strings"), 0, 3);
        this.add(numberOfStringsField, 1, 3);
        this.add(makeButton, 0, 4, 2, 1);
        this.add(outputArea, 0, 5, 2, 1);

        stringToChoseFromArea.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        stringLengthField.setText("20");
        numberOfStringsField.setText("10");
        GridPane.setHalignment(makeButton, HPos.CENTER);

        ColumnConstraints col20 = new ColumnConstraints();
        col20.setPercentWidth(30);
        ColumnConstraints col30 = new ColumnConstraints();
        col30.setPercentWidth(70);
        this.getColumnConstraints().addAll(col20, col30);
    }

    TextArea getStringToChoseFromArea() {
        return stringToChoseFromArea;
    }

    TextField getStringLengthField() {
        return stringLengthField;
    }

    TextField getNumberOfStringsField() {
        return numberOfStringsField;
    }

    Button getMakeButton() {
        return makeButton;
    }

    TextArea getOutputArea() {
        return outputArea;
    }
}
