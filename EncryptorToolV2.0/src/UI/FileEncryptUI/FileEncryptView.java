package UI.FileEncryptUI;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class FileEncryptView extends BorderPane {

    private GridPane topGrid;
    private ListView<String> fileListView;
    private TextArea messageArea;
    private Button selectFilesButton;
    private VBox dragDropTarget;

    private GridPane centerGrid;
    private Button encryptButton;
    private Button decryptButton;
    private CheckBox deleteAfterEnDeCrypt;
    private CheckBox bigFileCheckBox;
    private CheckBox onlyEnDecrypCheckBox;
    private Label statusLabel;
    private SplitPane centerSplitPane;

    private ProgressIndicator progressIndicator;

    private GridPane bottomGrid;

    public FileEncryptView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        //Top (drag'n'drop, list, messages)
        topGrid = new GridPane();

        centerSplitPane = new SplitPane();
        fileListView = new ListView<>();
        dragDropTarget = new VBox();
        selectFilesButton = new Button("Select file(s)");
        dragDropTarget.getChildren().addAll(new Label("Drag'n'drop files here to open or: "), selectFilesButton);
        dragDropTarget.setAlignment(Pos.CENTER);

        centerSplitPane.getItems().addAll(dragDropTarget, fileListView);
        messageArea = new TextArea("Messages here");
        messageArea.setEditable(false);
        messageArea.setWrapText(true);

        topGrid.add(centerSplitPane, 0, 0, 2, 1);
        topGrid.add(messageArea, 0, 1, 2, 1);

        //Center (options)
        centerGrid = new GridPane();
        encryptButton = new Button("Encrypt file(s)");
        decryptButton = new Button("Decrypt file(s)");
        deleteAfterEnDeCrypt = new CheckBox("Delete files after en-/decryption");
        bigFileCheckBox = new CheckBox("Big files");
        onlyEnDecrypCheckBox = new CheckBox("Only En-/Decrypt files that need en-/decryption");
        onlyEnDecrypCheckBox.setSelected(true);

        progressIndicator = new ProgressIndicator();

        centerGrid.add(encryptButton, 0, 0);
        centerGrid.add(decryptButton, 0, 1);
        centerGrid.add(deleteAfterEnDeCrypt, 1, 0);
        centerGrid.add(bigFileCheckBox, 1, 1);
        centerGrid.add(onlyEnDecrypCheckBox, 1, 2);

        //Bottom (status-bar)
        bottomGrid = new GridPane();
        statusLabel = new Label("Status");

        bottomGrid.add(statusLabel, 0, 0);

        //add things to the scene
        this.setCenter(new VBox(topGrid, centerGrid));
        this.setBottom(bottomGrid);
    }

    private void layoutNodes() {
        ColumnConstraints col50 = new ColumnConstraints();
        col50.setPercentWidth(50);

        //Bottom
        bottomGrid.setPadding(new Insets(2));
        bottomGrid.getColumnConstraints().addAll(col50, col50);
        bottomGrid.setAlignment(Pos.BOTTOM_CENTER);
        bottomGrid.setMinHeight(statusLabel.getHeight());

        //Center
        centerGrid.setPadding(new Insets(4, 8, 4, 8));
        centerGrid.setHgap(2);
        centerGrid.setVgap(4);
        centerGrid.setMinHeight(100);

        bigFileCheckBox.setTooltip(new Tooltip("Toggle this option on in case you're encrypting or decrypting big files.  \nBy default files are read into memory fully before encrypting them.  \nToggling this option on will cause files to be read and encrypted in smaller pieces.  \nThis is less secure but a better option nevertheless for big files."));


        progressIndicator.setMinWidth(75);
        progressIndicator.setMinHeight(75);
        GridPane.setHalignment(progressIndicator, HPos.CENTER);

        centerGrid.getColumnConstraints().addAll(col50, col50);

        //Top
        topGrid.setPadding(new Insets(4, 8, 4, 8));
        topGrid.setHgap(2);
        topGrid.setVgap(4);

        dragDropTarget.setMinWidth(150);
        fileListView.setMinWidth(150);

        topGrid.getColumnConstraints().addAll(col50, col50);
        GridPane.setVgrow(centerSplitPane, Priority.ALWAYS);

        messageArea.setMinHeight(100);
        messageArea.setPrefHeight(85);
        centerSplitPane.setMinHeight(150);

        GridPane.setVgrow(messageArea, Priority.ALWAYS);
        //GridPane.setVgrow(centerSplitPane, Priority.NEVER);

        dragDropTarget.setStyle("-fx-background-color: black;");
    }

    void toggleProgressIndicator(boolean enabled) {
        if (enabled) {
            centerGrid.getChildren().removeAll(encryptButton, decryptButton);
            centerGrid.add(progressIndicator, 0, 0, 1, 3);
        } else {
            centerGrid.getChildren().remove(progressIndicator);
            centerGrid.add(encryptButton, 0, 0);
            centerGrid.add(decryptButton, 0, 1);
        }
    }

    Button getSelectFilesButton() {
        return selectFilesButton;
    }

    ProgressIndicator getProgressIndicator() {
        return progressIndicator;
    }

    GridPane getTopGrid() {
        return topGrid;
    }

    ListView<String> getFileListView() {
        return fileListView;
    }

    TextArea getMessageArea() {
        return messageArea;
    }

    Button getEncryptButton() {
        return encryptButton;
    }

    Button getDecryptButton() {
        return decryptButton;
    }

    CheckBox getDeleteAfterEnDeCrypt() {
        return deleteAfterEnDeCrypt;
    }

    Label getStatusLabel() {
        return statusLabel;
    }

    SplitPane getCenterSplitPane() {
        return centerSplitPane;
    }

    CheckBox getBigFileCheckBox() {
        return bigFileCheckBox;
    }

    CheckBox getOnlyEnDecrypCheckBox() {
        return onlyEnDecrypCheckBox;
    }
}
