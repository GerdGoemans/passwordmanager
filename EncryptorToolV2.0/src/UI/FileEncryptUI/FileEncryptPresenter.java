package UI.FileEncryptUI;

import UI.UIConstants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import model.EncryptFilesTask;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static UI.UIConstants.*;


public class FileEncryptPresenter {

    private Stage currentStage;
    private FileEncryptView fileEncryptView;

    private List<File> fileList;
    private boolean askForDirs;

    public FileEncryptPresenter(Stage currentStage, FileEncryptView fileEncryptView) {
        this.currentStage = currentStage;
        this.fileEncryptView = fileEncryptView;

        fileList = new ArrayList<>();
        setMessageBoxText("");
        setStatusMessage("");
        updateView();
        addEventHandlers();

        currentStage.setMinWidth(600);
        currentStage.setMinHeight(400);
    }

    private void updateView() {
        try {
            ObservableList<String> fileObservableList = FXCollections.observableArrayList();
            this.fileList.forEach(file -> fileObservableList.add(file.getName()));
            this.fileEncryptView.getFileListView().setItems(fileObservableList);
        } catch (Exception e) {
            Alert alert = alert(Alert.AlertType.ERROR, "Error", "Something went wrong while updating the UI:", e.getMessage());
            alert.show();
        }
    }

    private void addEventHandlers() {
        this.fileEncryptView.getDecryptButton().setOnAction(event -> {
            if (enDeCryptSetup(event, "Something went wrong while decrypting your files")) return;

            EnDeCryptEvent(false, "Decrypting");
        });
        this.fileEncryptView.getEncryptButton().setOnAction(event -> {
            if (enDeCryptSetup(event, "Something went wrong while encrypting your files")) return;

            EnDeCryptEvent(true, "Encrypting");
        });

        this.fileEncryptView.getSelectFilesButton().setOnAction(event -> {
            try {
                List<File> tempList = UIConstants.openFile(event, "", "", currentStage);
                if (tempList == null || tempList.size() < 1) return;
                askForDirs = true;
                tempList.forEach(this::addFileToFileList);
                updateView();
            } catch (IOException e) {
                Alert alert = alert(Alert.AlertType.ERROR, "Error", "Something went wrong", e.getMessage());
                alert.show();
            }
        });
        this.fileEncryptView.getCenterSplitPane().setOnDragOver(event -> {
            if (event.getGestureSource() != this.fileEncryptView.getCenterSplitPane()
                    && event.getDragboard().hasFiles()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.LINK);
            }
            event.consume();
        });
        this.fileEncryptView.getCenterSplitPane().setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                askForDirs = true;
                db.getFiles().forEach(this::addFileToFileList);
            }
            updateView();
        });
    }

    private void EnDeCryptEvent(boolean isEncryptEvent, String enOrDeCrypt) {

        EncryptFilesTask encryptFilesTask = new EncryptFilesTask(fileList, getMasterPassword(), getEnDecryptor(), isEncryptEvent, this.fileEncryptView.getDeleteAfterEnDeCrypt().isSelected(), this.fileEncryptView.getBigFileCheckBox().isSelected(), this.fileEncryptView.getOnlyEnDecrypCheckBox().isSelected());

        setMessageBoxText("");
        setStatusMessage("");

        this.fileEncryptView.getStatusLabel().textProperty().unbind();
        this.fileEncryptView.getStatusLabel().textProperty().bind(encryptFilesTask.messageProperty());

        this.fileEncryptView.toggleProgressIndicator(true);
        this.fileEncryptView.getProgressIndicator().progressProperty().unbind();
        this.fileEncryptView.getProgressIndicator().progressProperty().bind(encryptFilesTask.progressProperty());

        encryptFilesTask.messageProperty().addListener((observable, oldValue, newValue) -> {
            this.appendMessageBoxText(newValue);
            this.fileEncryptView.getMessageArea().setScrollTop(Double.MAX_VALUE);
        });

        encryptFilesTask.addEventFilter(WorkerStateEvent.WORKER_STATE_SUCCEEDED, event1 -> {
            this.fileEncryptView.toggleProgressIndicator(false);
            this.fileEncryptView.getStatusLabel().textProperty().unbind();
            this.fileEncryptView.getProgressIndicator().progressProperty().unbind();
            this.setStatusMessage(enOrDeCrypt + " Succeeded");
            this.appendMessageBoxText("\n\n");
            encryptFilesTask.getValue().forEach(this::appendMessageBoxText);
            this.fileList.clear();
            updateView();
            setMasterPassword("");

            windowNotification(currentStage);
            currentStage.sizeToScene();
        });
        encryptFilesTask.addEventFilter(WorkerStateEvent.WORKER_STATE_CANCELLED, event1 -> {
            this.setStatusMessage(enOrDeCrypt + " canceled");
            this.fileEncryptView.toggleProgressIndicator(false);
            this.fileEncryptView.getStatusLabel().textProperty().unbind();
            this.fileEncryptView.getProgressIndicator().progressProperty().unbind();
            this.appendMessageBoxText("\n\n");
            encryptFilesTask.getValue().forEach(this::appendMessageBoxText);
            windowNotification(currentStage);
            currentStage.sizeToScene();
        });
        encryptFilesTask.addEventFilter(WorkerStateEvent.WORKER_STATE_FAILED, event1 -> {
            this.setStatusMessage(enOrDeCrypt + " failed");
            this.fileEncryptView.toggleProgressIndicator(false);
            this.fileEncryptView.getStatusLabel().textProperty().unbind();
            this.fileEncryptView.getProgressIndicator().progressProperty().unbind();
            this.appendMessageBoxText("\n\n");
            encryptFilesTask.getValue().forEach(this::appendMessageBoxText);
            windowNotification(currentStage);
            currentStage.sizeToScene();
        });

        Thread thread = new Thread(encryptFilesTask);
        thread.start();
    }

    private boolean enDeCryptSetup(ActionEvent event, String errorMessage) {
        if (fileList == null || fileList.size() < 1) {
            event.consume();
            return true;
        }
        boolean returnVal = false;
        try {
            returnVal = askUserForPassword();
        } catch (NoSuchAlgorithmException e) {
            Alert alert1 = alert(Alert.AlertType.ERROR, "ERROR", errorMessage, e.getMessage());
            alert1.show();
        }

        return !returnVal;
    }

    @SuppressWarnings("ConstantConditions")
    private void addFileToFileList(File file) {
        if (fileList.contains(file)) return;

        if (file.isDirectory()) {
            if (file.listFiles() == null) return;

            if (askForDirs) {
                Alert alert = alert(Alert.AlertType.CONFIRMATION, "Directory detected", "A directory was detected in the selected list", "Do you want to ignore this directory, add the files of the directory only or add the files from this directory and all its subdirectories?");
                alert.getButtonTypes().clear();
                ButtonType ignore = new ButtonType("Ignore");
                ButtonType thisSubOnly = new ButtonType("Files from this subdirectory only");
                ButtonType allSubDirs = new ButtonType("Files from all subdirectories");
                alert.getButtonTypes().addAll(ignore, thisSubOnly, allSubDirs);
                alert.showAndWait();

                ButtonType result = alert.getResult();

                if (result.equals(allSubDirs)) {
                    askForDirs = false;
                    for (File subDirFile : file.listFiles()) {
                        addFileToFileList(subDirFile);
                    }
                } else if (result.equals(thisSubOnly)) {
                    for (File subDirFile : file.listFiles()) {
                        if (subDirFile.isFile()) {
                            this.fileList.add(subDirFile);
                        }
                    }
                }
            } else {
                for (File subDirFile : file.listFiles()) {
                    addFileToFileList(subDirFile);
                }
            }
        } else {
            this.fileList.add(file);
        }
    }

    private void setStatusMessage(String text) {
        if (text == null) return;
        fileEncryptView.getStatusLabel().setText(text);
    }

    private void setMessageBoxText(String text) {
        TextArea textArea = this.fileEncryptView.getMessageArea();
        if (text == null || text.equals("")) {
            this.fileEncryptView.getTopGrid().getChildren().remove(textArea);
            this.fileEncryptView.getMessageArea().setText("");
        } else if (this.fileEncryptView.getTopGrid().getChildren().contains(textArea)) {
            this.fileEncryptView.getMessageArea().setText(text);
        } else {
            this.fileEncryptView.getTopGrid().add(textArea, 0, 1, 2, 1);
            this.fileEncryptView.getMessageArea().setText(text);

            this.fileEncryptView.getCenterSplitPane().setMinHeight(85);
            this.fileEncryptView.getCenterSplitPane().setPrefHeight(85);
            this.fileEncryptView.getCenterSplitPane().setMaxHeight(85);
        }
        currentStage.sizeToScene();
    }

    private void appendMessageBoxText(String text) {
        if (text == null || text.equals("")) return;
        TextArea textArea = this.fileEncryptView.getMessageArea();

        if (textArea.getText().equals("")) {
            setMessageBoxText(text);
        } else {
            String oldText = textArea.getText();
            textArea.setText(String.format("%s %s", oldText, text));
        }
    }

}
