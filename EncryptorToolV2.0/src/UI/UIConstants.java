package UI;

import UI.InputUI.InputPresenter;
import UI.InputUI.InputView;
import UI.InputUI.InputViewType;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.EnDecryptor;
import model.FileFunctions;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public abstract class UIConstants {

    private static String masterPassword = "";
    private static byte[] masterPasswordHash;
    private static byte[] masterPasswordSalt;

    private static EnDecryptor enDecryptor;
    private static Stage primaryStage;
    private static Scene mainScene;

    /*Other methods*/
    /*private*/
    private static void setupEnDecryptor() {
        if (enDecryptor == null) enDecryptor = new EnDecryptor();
    }

    /*Other methods*/
    /*public*/
    public static void setupStage(Stage stage) {
        stage.setTitle("Encryptor-tool");
        stage.getIcons().add(new Image("/images/data-encryption.png"));

        if (stage.getScene() != null && !stage.isMaximized()) {
            stage.sizeToScene();
        }

        stage.setOnCloseRequest(event -> {
            if (stage != primaryStage) return;

            Alert alert = alert(Alert.AlertType.CONFIRMATION, "Exit", "You're about to close the program", "Nothing will be saved, are you sure you want to close the program?");
            alert.getButtonTypes().clear();
            ButtonType exit = new ButtonType("Exit");
            ButtonType cancel = new ButtonType("Cancel");
            alert.getButtonTypes().addAll(cancel, exit);
            alert.showAndWait();

            if (alert.getResult().equals(exit)) {
                stage.close();
            }

            if (alert.getResult().equals(cancel)) {
                event.consume();
                return;
            }
            event.consume();
        });
    }

    public static Alert alert(Alert.AlertType alertType, String title, String headerText, String contextText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        if (contextText != null && !contextText.equals("")) {
            alert.setContentText(contextText);
        }
        alert.getDialogPane().getStylesheets().add("CSS/General.css");

        return alert;
    }

    public static boolean askUserForPassword() throws NoSuchAlgorithmException {
        if (!masterPassword.equals("")) return true;

        Stage passwordStage = new Stage();
        InputView inputView = new InputView("Enter master password: ", InputViewType.password);
        new InputPresenter(inputView, passwordStage);
        Scene inputScene = new Scene(inputView);
        passwordStage.setScene(inputScene);
        setupStage(passwordStage);
        inputScene.getStylesheets().add("CSS/General.css");

        passwordStage.showAndWait();

        if (masterPassword.equals("")) return false;

        setupEnDecryptor();

        byte[] tempHashPW = enDecryptor.hashMasterPassword(masterPassword.getBytes(UTF_8), getMasterPasswordSalt());
        if (!Arrays.equals(tempHashPW, getMasterPasswordHash())) {
            Alert alert = alert(Alert.AlertType.WARNING, "Wrong master password", "Entered master password was wrong", "");
            alert.show();
            masterPassword = "";
            return false;
        }
        return true;
    }

    public static List<File> openFile(ActionEvent event, String extentionName, String extiontion, Stage currentStage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open one or more files");
        fileChooser.setInitialDirectory(FileFunctions.getSaveLocation().toFile());
        if (extentionName.equals("")) {
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("All files", "*.*"));
        } else {
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter(extentionName, extiontion),
                    new FileChooser.ExtensionFilter("All files", "*.*"));
        }

        List<File> filesList = fileChooser.showOpenMultipleDialog(currentStage);
        if (filesList == null) {
            event.consume();
            return null;
        }
        return filesList;
    }

    public static void windowNotification(Stage stage) {
        if (!stage.isFocused()) {
            final Runnable runnable = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.default");
            if (runnable != null) {
                runnable.run();
            }
            stage.toFront();
        }
    }

    /*Getters and setters*/
    public static String getMasterPassword() {
        return masterPassword;
    }

    public static void setMasterPassword(String masterPassword) {
        UIConstants.masterPassword = masterPassword;
    }

    private static byte[] getMasterPasswordHash() {
        return masterPasswordHash;
    }

    public static void setMasterPasswordHash(byte[] masterPasswordHash) {
        UIConstants.masterPasswordHash = masterPasswordHash;
    }

    private static byte[] getMasterPasswordSalt() {
        return masterPasswordSalt;
    }

    public static void setMasterPasswordSalt(byte[] masterPasswordSalt) {
        UIConstants.masterPasswordSalt = masterPasswordSalt;
    }

    public static void setPrimaryStage(Stage primaryStage) {
        UIConstants.primaryStage = primaryStage;
    }

    public static EnDecryptor getEnDecryptor() {
        if (enDecryptor == null) setupEnDecryptor();
        return enDecryptor;
    }

    public static Scene getMainScene() {
        return mainScene;
    }

    public static void setMainScene(Scene mainScene) {
        if (!mainScene.getStylesheets().contains("CSS/General.css")) {
            mainScene.getStylesheets().add("CSS/General.css");
        }
        UIConstants.mainScene = mainScene;
    }
}
