import UI.TabPaneUI.TabPanePresenter;
import UI.TabPaneUI.TabPaneView;
import UI.UIConstants;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        Thread.currentThread().setName("EncryptorTool");
        launch(args);
    }

    public void start(Stage primaryStage) {
        primaryStage.setTitle("EncryptorTool");
        try {
            UIConstants.setupStage(primaryStage);

            TabPaneView tabPaneView = new TabPaneView(primaryStage);
            Scene mainScene = new Scene(tabPaneView);
            new TabPanePresenter(tabPaneView, primaryStage);
            UIConstants.setMainScene(mainScene);
            primaryStage.setMinWidth(600);
            primaryStage.setMinHeight(500);
            primaryStage.setScene(mainScene);
            primaryStage.centerOnScreen();
            primaryStage.show();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("An unhandled exception occurred causing a crash.");
            alert.setContentText(e.getMessage());
            e.printStackTrace();
        }
    }
}